import pandas as pd
import numpy as np
from os.path import join
from sklearn.experimental import enable_iterative_imputer
from sklearn.impute import IterativeImputer
from sklearn.model_selection import StratifiedKFold
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.cm as cm
from matplotlib.colors import LinearSegmentedColormap
import shap

mpl.rcParams["scatter.marker"] = '+'
cmap = LinearSegmentedColormap.from_list('style1_cmap', ['#316D9C', '#E19CF1', '#B50724'])



def sigmoid(x):
    return 1 / (1 + np.exp(-x))

def correct_imputed_values(x):
    for labtest, disease, threshold in (['HbA1c', 'Diabetes', 6.4],
                                    ['LDL', 'Dyslipidemia', 3.3],
                                    ['Creatinine', 'CKD', 200]
                                    ):
        values = x[labtest]
        has_disease = x[disease]
        values_to_clip = values[has_disease == 0].copy()
        new_values = values_to_clip.clip(upper=threshold)
        new_values = new_values.round(1)
        values[has_disease == 0] = new_values
        x[labtest] = values

    return x

def impute_labtest_values(x):
    c = ['Diabetes', 'Hypertension', 'Dyslipidemia',
            'CKD', 'age', 'HbA1c', 'LDL', 'Creatinine']
    x_to_impute = x[c]
    x_remaining = x[[_ for _ in x.columns if _ not in c]]
    imputer = IterativeImputer()
    x_imputed = imputer.fit_transform(x_to_impute)
    x_imputed = pd.DataFrame(x_imputed, columns=c)
    x_imputed = correct_imputed_values(x_imputed)
    x_new = pd.concat((x_remaining, x_imputed), axis=1)
    return x_new

def get_oof_predictions(model, x, y):
    skf = StratifiedKFold(n_splits=5)
    to_return = np.zeros_like(y).astype(float)
    for train_index, test_index in skf.split(x, y):
        x_train, x_test = x[train_index], x[test_index]
        y_train, y_test = y[train_index], y[test_index]
        model.fit(x_train, y_train)
        y_pred = model.predict_proba(x_test)
        y_pred = y_pred[:, 1]
        to_return[test_index] = y_pred

    return to_return


def get_shap_values(model, predictors, yt, fname_to_save=None):
    explainer = shap.TreeExplainer(model)
    shap_values = explainer.shap_values(predictors.values)
    shap_values_df = pd.DataFrame(shap_values, index=predictors.index,
                                    columns=predictors.columns)
    shap_values_df['base_value'] = explainer.expected_value
    if fname_to_save is not None:
        shap_values_df.to_csv(fname_to_save)
    return shap_values_df

def convert_shap_to_partial_risk(shap):
    shap_sum = shap.sum(axis=1)
    total_risk = sigmoid(shap_sum)
    partial_risk = np.zeros_like(shap)
    for column in range(partial_risk.shape[1]):
        risk_without_column = sigmoid(shap_sum - shap[:, column])
        partial_risk[:, column] = total_risk - risk_without_column
    ## convert to percentage
    return partial_risk * 100

def plot_beeswarm(impact, predictors, xlabel, fname):
    shap.summary_plot(impact, predictors, sort=False, alpha=0.5, show=False)
    fig = plt.gcf()
    ax, ax2 = fig.axes
    scatplot = ax.collections
    for s in scatplot:
        s.update_scalarmappable()
        s.set_cmap(cmap)
        s.set_linewidth(None)

    fig.set_size_inches(6,6)
    # ax.spines['top'].set_visible(False)
    # ax.spines['top'].set_color('gray')
    ax.xaxis.grid(True, color='0.85', ls='dotted', lw=0.5)
    # ax.xaxis.set_ticks_position('top')
    ax.tick_params(axis='both', which='both', length=0)
    ax.set_xlabel(xlabel)

    cbar = ax2.collections
    for s in cbar:
        s.update_scalarmappable()
        s.set_cmap(cmap)
        ax2.set_ylabel('Feature value', labelpad=-15)

    plt.tight_layout()
    plt.savefig(fname, dpi=300)
    plt.close('all')
