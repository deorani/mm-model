# import os
# os.environ['KERAS_BACKEND'] = 'plaidml.keras.backend'
import tensorflow as tf
tf.random.set_seed(10)

import pandas as pd
import numpy as np
import shap
from os.path import join
from sklearn.experimental import enable_iterative_imputer
from sklearn.impute import IterativeImputer
from sklearn.model_selection import train_test_split, KFold
from sklearn.metrics import roc_auc_score
from keras import Model, Input, constraints, optimizers
from keras.layers import Dense, BatchNormalization, Dropout, Activation, Add
import keras.backend as K
from utils import impute_labtest_values, get_oof_predictions, read_data
import random
import time


random.seed(10)
np.random.seed(10)


tic = time.time()


def get_sample_weights(outcome):
    ## value = 1 (existing disease) should have a weight = 0
    return 1 - x[outcome].values



def read_data():
    '''function to read data and xgb predictions'''

    return x, y


def get_auc_scores(truth, prediction):
    scores = {}
    for column in truth.columns:
        w = sample_weights[column.rsplit('_',1)[0][:12]
                            + '_'
                            + column.split('_')[-1]
                            ].astype(bool)
        y_true = truth[column][w].values
        y_pred = prediction[column][w].values
        scores[column] = roc_auc_score(y_true, y_pred)

    return pd.Series(scores)

def custom_logloss(y_true, y_pred):
    not_existing_disease_mask = y_true >= 0
    loss = K.binary_crossentropy(y_true, y_pred)
    loss = loss[not_existing_disease_mask]
    return K.mean(loss)

def get_model(n_inputs, n_outputs, lr):

    main_input = Input(shape=(n_inputs,))
    x1 = Dense(32)(main_input)
    x2 = BatchNormalization()(x1)
    x3 = Activation('sigmoid')(x2)
    x4 = Dense(16)(x3)
    x5 = BatchNormalization()(x4)
    x6 = Activation('sigmoid')(x5)

    outputs = []
    for outcome in outcome_names:
        incremental_dense_layers = []
        for op in range(1, 1+n_outputs):
            layer_name = '{0}_incremental_{1}'.format(outcome[:5], op)
            if op == 1:
                x7 = Dense(8)(x6)
                ## first output may have a negative value
                x8 = Dense(1, name=layer_name)(x7)
                incremental_dense_layers.append(x8)
                add_dense = x8
            else:
                x7 = Dense(4)(x6)
                ## all others output must have a positive value, for incremental risk
                x8 = Dense(1, name=layer_name, activation='sigmoid')(x7)
                incremental_dense_layers.append(x8)
                layer_name = '{0}_addlayer_{1}'.format(outcome[:5], op)
                add_dense = Add(name=layer_name)(incremental_dense_layers)

            final_output_name = 'output_{0}_{1}'.format(outcome[:5], op)
            output = Activation('sigmoid', name=final_output_name)(add_dense)
            outputs.append(output)

    sgd = optimizers.SGD(lr=lr, decay=1e-4)
    adam = optimizers.Adam(lr=lr)
    model = Model(inputs=main_input, outputs=outputs)
    model.compile(optimizer=sgd, loss='binary_crossentropy')

    return model


outcome_names = ['Cerebrovascular disease', 'Ischemic heart disease', 'CKD', 'CHF']
num_outputs = len(outcome_names)

sample_weights = {}
for outcome in outcome_names:
    for op in range(1, 11):
        sample_weights['output_{0}_{1}'.format(outcome[:5], op)] = get_sample_weights(outcome)


x, y = read_data()

kf = KFold(n_splits=5)
auc_scores = {}
lr = 0.01
epochs = 100
batch_size = 32
oof_predictions = pd.DataFrame(index=y.index, columns=y.columns, data=0.0)
for train_index, test_index in kf.split(x, y):
    x_train, x_test = x.iloc[train_index], x.iloc[test_index]
    y_train, y_test = y.iloc[train_index], y.iloc[test_index]
    y_train = [y_train[c].values for c in y_train.columns]
    y_test = [y_test[c].values for c in y_test.columns]

    model = get_model(x.shape[1], 10, lr)
    model.fit(x_train, y_train, batch_size=batch_size, epochs=epochs,
                    validation_data=(x_test, y_test), verbose=0)
    y_pred = model.predict(x_test)
    y_pred = np.squeeze(y_pred).T
    oof_predictions.iloc[test_index] = y_pred
    del model
    # print('  --> {:0.1f} seconds'.format(time.time() - tic))

model = get_model(x.shape[1], 10, lr)
model.fit(x, [y[c].values for c in y.columns],
           batch_size=batch_size, epochs=epochs, verbose=0)
model_fname = 'lr={0}_bs={1}_ep={2}_nn_model.h5'.format(lr, batch_size, epochs)
model.save(join('models', model_fname))

preds_fname = 'lr={0}_bs={1}_ep={2}_preds.csv'.format(lr, batch_size, epochs)
oof_predictions.to_csv(join('results', 'nn_predictions', preds_fname))

name = 'lr={0}_bs={1}_ep={2}'.format(lr, batch_size, epochs)
scores = get_auc_scores(y, oof_predictions)
auc_scores[name] = scores
print(name, '{:0.1f} seconds'.format(time.time() - tic))
print(scores.sum())


auc_scores = pd.DataFrame(auc_scores)
auc_scores.to_csv(join('results', 'nn_auc_scores.csv'))
