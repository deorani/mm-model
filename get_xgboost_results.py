import json
import time
import pandas as pd
import numpy as np
import xgboost as xgb
import os
import pickle
from os.path import join, exists
import matplotlib.pyplot as plt
from utils import (impute_labtest_values, get_oof_predictions, get_shap_values,
                    convert_shap_to_partial_risk, plot_beeswarm)



def read_data():
    '''function to read data'''

    return x, y


def plot_feature_impact(model, predictors, yt):
    fname = '{0}_shap_values.csv'.format(yt.name)
    fname = join('results', 'xgb_shap_values', fname)
    shap_values = get_shap_values(model, predictors, yt, fname)
    ## remove the disease from beeswarm
    columns_to_plot = [c for c in x.columns if c != yt.name] + ['base_value']
    # columns_to_plot = columns  + ['base_value']
    shap_values = shap_values[columns_to_plot]
    partial_risk = convert_shap_to_partial_risk(shap_values.values)

    shap_values.pop('base_value')
    to_plot = (shap_values * null_location).values
    fname = fname.replace('.csv', '.png')
    xlabel = 'SHAP value (impact on model output)'
    plot_beeswarm(to_plot, predictors, xlabel, fname)

    to_plot = (partial_risk[:,:-1] * null_location).values
    fname = fname.replace('shap_values.png', 'partial_risk.png')
    xlabel = 'Incremental risk for {0} (%)'.format(columns_rename[yt.name])
    plot_beeswarm(to_plot, predictors, xlabel, fname)



plt.ioff()

shap_op = join('results', 'xgb_shap_values')
if not exists(shap_op):
    os.mkdir(shap_op)

params = json.load(open('xgboost_model_params.json', 'r'))

x, y = read_data()

output_names = ['Cerebrovascular disease', 'Ischemic heart disease', 'CKD', 'CHF']
tvals = list(range(182, 1826, 182))

all_scores = pd.DataFrame(index=tvals, columns=['{0}_{1}'.format(op, suffix)
                                                for op in output_names
                                                for suffix in ['before', 'after']
                                                ]
                           )
tic = time.time()
for op in output_names:
    targets = y[op]
    index = (y[op] > 0) | y[op].isnull()
    targets = targets[index]
    predictors = x[index]
    null_location = full_null_location[index]

    ## remove disease as input : all values are zero
    predictors.pop(op)
    null_location.pop(op)

    pred_vals = []
    for t in tvals:
        yt = (targets.notnull() & (targets <= t)).astype(int)
        this_params = params[op][str(t)]
        model = xgb.XGBClassifier(**this_params)

        y_pred = get_oof_predictions(model, predictors.values, yt.values)
        pred_vals.append(y_pred)

        model.fit(predictors.values, yt.values)
        model_fname = '{0}_t={1}_model.pkl'.format(yt.name, t)
        model_fname = join('models', model_fname)
        pickle.dump(model, open(model_fname, 'wb'))
        # plot shap values
        if t == 1820:
            model = xgb.XGBClassifier(**this_params)
            model.fit(predictors.values, yt.values)
            plot_feature_impact(model, predictors, yt)


    pred_vals = np.array(pred_vals).T
    pred_vals = pd.DataFrame(data=pred_vals, index=predictors.index, columns=tvals)
    pred_vals.to_csv(join('results', '{0}_xgb_preds.csv'.format(op.split()[0])))
    print('time elapsed : {0:.2f}'.format(time.time() - tic))
