import json
import pandas as pd
import numpy as np
from os.path import join
from sklearn.model_selection import (train_test_split,
                    cross_val_score, GridSearchCV, RandomizedSearchCV)
from sklearn.metrics import roc_auc_score, log_loss
from sklearn.ensemble import RandomForestClassifier
import xgboost as xgb
from utils import impute_labtest_values


def read_data():
    '''function to read data'''

    return x, y

def get_best_model(x, y):
    model = xgb.XGBClassifier(n_jobs=6)
    params = {'n_estimators'  : [50, 100, 300, 600],
              'learning_rate' : [0.001, 0.01, 0.05],
               'min_child_weight' : [1, 3, 5],
               'gamma'        : [0, 0.5, 1],
               'max_depth'    : [3, 4]
               }
    best_model = RandomizedSearchCV(model, params, n_iter=20, scoring='roc_auc', cv=5)
    best_model.fit(x,y)
    return best_model


x, y = read_data()
output_names = ['Cerebrovascular disease', 'Ischemic heart disease', 'CKD', 'CHF']
tvals = list(range(182, 1826, 182))

params = {}

for op in output_names:
    targets = y[op]
    index = (y[op] > 0) | y[op].isnull()
    targets = targets[index]
    predictors = x[index]

    params[op] = {}

    for t in tvals:
        yt = (targets.notnull() & (targets <= t)).astype(int)
        best_model = get_best_model(predictors.values, yt.values)
        print(op, ', at time = {0} days'.format(t))
        print('score = {0}'.format(best_model.best_score_))
        this_params = best_model.best_estimator_.get_params()
        # print(this_params)
        params[op][t] = this_params


json.dump(params, open('xgboost_model_params.json', 'w'), indent=4)
